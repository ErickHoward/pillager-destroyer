using System.Collections;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] [Range(0.1f,10)] float enemySpawnWaitTime = 1f;
    [SerializeField] [Range(0,50)] int poolSize = 5;
    [SerializeField] GameObject enemyPrefab;

    GameObject[] pool;

    private void Awake() 
    {
        PopulatePool(enemyPrefab);
    }

    void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    
    private void PopulatePool(GameObject prefab)
    {
        pool = new GameObject[poolSize];

        for (int i = 0; i < pool.Length; i++)
        {
            pool[i] = Instantiate(prefab, transform);
            pool[i].SetActive(false);

        }
    }


    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            EnableObjectInPool();
            yield return new WaitForSeconds(enemySpawnWaitTime);
        }
    }

    private void EnableObjectInPool()
    {
        for (int i = 0; i < pool.LongLength; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                pool[i].SetActive(true);
                return;
            }
        }
    }
}
